#ifndef SUANFA_H
#define SUANFA_H
#include <QString>
#include <QDebug>
#include "struct_num.h"
class suanfa
{
public:
    suanfa();

    /**
     * @description: 生成随机数
     * @param {type} 随机数的位数
     * @return: 随机数
     */    
    int random(int coms);
    /**
     * @description: 随机生成符号
     * @return: 符号
     */
    QString num_sign();
    /**
     * @description: 判断两个数与结果是否相等
     * @param {type} 运算的结构体
     * @return: bool
     */
    bool ans_istrue(numgroup *png);
};

#endif // SUANFA_H
