#include "mainwindow.h"
#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //引入样式表文件
    QFile qss(":style.qss");
    if(! qss.open(QFile::ReadOnly)) {qDebug()<<"Opening qss file failed!"<<endl; return a.exec();}
    QString qsty = QLatin1String(qss.readAll());
    a.setStyleSheet(qsty);
    qss.close();


    MainWindow w;
    w.show();

    return a.exec();
}
