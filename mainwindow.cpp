#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QtMath>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //固定窗口大小
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);

    //初始化的时就开始生成数字
    genter_num(ng);
//    添加快捷键:回车
    ui->sure_btn->setShortcut(tr("Enter"));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_sure_btn_clicked()
{
    ng->ans = ui->ans_le->text().toInt();
    suanfa sf;

//    if(sf.ans_istrue(ng)) qDebug("True\n");
//    else qDebug("false\n");

    if (sf.ans_istrue(ng)){ui->true_lab->setVisible(true); ui->false_lab->setVisible(false);}
    else {ui->true_lab->setVisible(false); ui->false_lab->setVisible(true);}

    genter_num(ng);
}

void MainWindow::genter_num(numgroup *png)
{
//    qDebug("Entering genter_num\n");
    suanfa sf;
re_genternum:
    png->f_num = sf.random(2);
    png->s_num = sf.random(2);
    png->number_sign = sf.num_sign();

    //对于生成数字的逻辑进行判断
    if(!MainWindow::is_true_number()) goto re_genternum;

//    qDebug("f_num:%d s_num:%d\n",png->f_num,png->s_num);
//    qDebug()<<"Sign:"<<png->number_sign<<" num_sign:"<<thnum;

    ui->f_num->setNum(png->f_num);
    ui->s_num->setNum(png->s_num);
    ui->sign_lab->setText(png->number_sign);
}

bool MainWindow::is_true_number()
{
    if (thnum == 4 && ng->s_num == 0 ) return false; //确定除数不等于0
    if (thnum == 4 && ng->f_num % ng->s_num != 0) return false; //判断是否可以整除
    if (thnum == 2 && ng->f_num - ng->s_num < 0) return false; //保证不会计算出负数
    return true;
}
