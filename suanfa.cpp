/*
 * @Description: 
 * @Author: diyhome
 * @Gitee: https://gitee.com/jingjiangxueyuan_hmqs
 * @Date: 2020-01-20 10:22:13
 * @LastEditors  : diyhome
 * @LastEditTime : 2020-01-20 10:49:45
 */
#include "suanfa.h"
#include <cstdlib>
#include <QtMath>

int thnum;

suanfa::suanfa()
{
}

int suanfa::random(int coms)
{
//    qDebug("random number process!");
    return rand() % (int)(pow(10,coms));
}

QString suanfa::num_sign()
{
    QString si;
    int _random = rand() % 4 + 1;
    switch (_random) {
    case 1: si = "+"; break;
    case 2: si = "-"; break;
    case 3: si = "×"; break;
    case 4: si = "÷"; break;
    }
//    qDebug()<<"number sign is"<<si<<endl;
    thnum = _random;
    return si;
}

bool suanfa::ans_istrue(numgroup *png)
{
    bool tmp = true;
    switch (thnum) {
    case 1: tmp = (png->f_num + png->s_num == png->ans) ? true : false; break;
    case 2: tmp = (png->f_num - png->s_num == png->ans) ? true : false; break;
    case 3: tmp = (png->f_num * png->s_num == png->ans) ? true : false; break;
    case 4: tmp = (png->f_num / png->s_num == png->ans) ? true : false; break;
    }
//    qDebug()<<"answer is:"<<tmp;
    return tmp;
}
