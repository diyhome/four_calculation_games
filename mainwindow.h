#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include "suanfa.h"
#include "struct_num.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //声明一个结构体指针并初始化
    numgroup *ng = new numgroup;

    MainWindow(QWidget *parent = nullptr);
    void genter_num(numgroup *png);
    ~MainWindow();

private slots:
    void on_sure_btn_clicked();

private:
    Ui::MainWindow *ui;

    //判断题目是否符合规则
    bool is_true_number();
};
#endif // MAINWINDOW_H
