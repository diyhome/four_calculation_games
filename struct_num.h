#ifndef STRUCT_NUM_H
#define STRUCT_NUM_H
#include <QString>
//定义一个公共的结构体
struct numgroup{
    int f_num;
    int s_num;
    int ans;
    QString number_sign;
};
//声明一个全局变量用来记录现在是用哪一个符号
extern int thnum;
#endif // STRUCT_NUM_H
